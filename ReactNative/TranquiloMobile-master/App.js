import React from "react"
import { ThemeProvider } from "styled-components"

import AppContainer, { Navigator } from "./src/navigation"
import { Theme } from "@styles"

export default class App extends React.Component {
  render() {
    return (
      <ThemeProvider theme={Theme}>
        <AppContainer
          ref={navigatorRef => {
            Navigator.setTopLevelNavigator(navigatorRef)
          }}
        />
      </ThemeProvider>
    )
  }
}
