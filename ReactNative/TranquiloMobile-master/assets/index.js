export default {
  logo: require("./images/logo.png"),
  start: require("./images/start.png"),
  stop: require("./images/stop.png"),
  batteryIndicators: {
    0: require("./images/Batt-00.png"),
    1: require("./images/Batt-10.png"),
    2: require("./images/Batt-20.png"),
    3: require("./images/Batt-30.png"),
    4: require("./images/Batt-40.png"),
    5: require("./images/Batt-50.png"),
    6: require("./images/Batt-60.png"),
    7: require("./images/Batt-70.png"),
    8: require("./images/Batt-80.png"),
    9: require("./images/Batt-90.png"),
    10: require("./images/Batt-100.png")
  }
}
