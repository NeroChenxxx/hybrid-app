import { Platform, PermissionsAndroid } from 'react-native'
import { BleManager, ScanMode } from "react-native-ble-plx"
import base64 from "react-native-base64"

import {
  SERVICE_UUID,
  CHARACTERISTIC_UUID,
  CHARACTERISTIC_UUID_BATTERY
} from "@constants"

/**
 * ACCESS_COARSE_LOCATION Permission needed for android above version 23 when use bluetooth
 * Also, need to turn on the location service on android phone
 */

export async function registerForCoarseLocationAsync() {
  if (Platform.OS === "android" && Platform.Version >= 23) {
    const status = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION)

    if (status === PermissionsAndroid.RESULTS.DENIED) return false
  } else if (Platform.OS === "android" && Platform.Version >= 29) {
    const status = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)

    if (status === PermissionsAndroid.RESULTS.DENIED) return false
  }

  return true
}

export const bleManger = new BleManager()
export let bleDevice = null

export const connectToDevice = async(device) => {
  return device 
          .connect({
            autoConnect: false
          })
          .then(device => device.requestMTU(100))
          .then(device => device.discoverAllServicesAndCharacteristics())
}

export function scanAndConnect() {
  return Promise.race([
    new Promise((resolve, reject) => {
      bleManger.startDeviceScan(
        Platform.OS === "ios" ? [SERVICE_UUID] : null,
        { scanMode: ScanMode.LowLatency }, // In this mode, Android scans continuously. Obviously this uses the most power, but it will also guarantee the best scanning results.
        (error, device) => {
          console.log("Scanning.......", device && device.name)
          if (error) {
            reject(error)
          }

          if (
            device &&
            device.name &&
            device.name.startsWith("Tranquilo Mat")
          ) {
            console.log("find device:", device && device.name)
            bleManger.stopDeviceScan()

            connectToDevice(device)
              .then(device => {
                bleDevice = device  //In android, bleDevice is a BluetoothGatt object
                resolve(device)
              })
              .catch(error => reject(error))
          }
        }
      )
    }),
    new Promise((resolve, reject) =>
      setTimeout(() => { //future problem: racing between timeout and connectedDevice
        bleManger.stopDeviceScan()
        reject(`Pairing Time Out!`)
      }, 1000 * 20)
    )
  ])
}

export function disconnect() {
  console.log("disconnect,,,,,,,,,,")
  if(!bleDevice) return Promise.reject()

  return checkBleConection().then(isConnected => {
    if(isConnected) bleDevice.cancelConnection()
  })
}

export function checkBleConection() {
  if(!bleDevice) return Promise.reject()

  return bleDevice.isConnected()
}

function task(cb) {
  if (!bleDevice) return false

  return cb()
}

function send(command) {
  return task(() => {
    return bleDevice.writeCharacteristicWithResponseForService( //In android, this is a BluetoothGattCallback function
      SERVICE_UUID,
      CHARACTERISTIC_UUID,
      command
    )
  })
}

export function notify(cb, charUUID) {
  return task(() => {
    return bleDevice.monitorCharacteristicForService(SERVICE_UUID, charUUID, cb)
  })
}

export function readBattery() {
  return task(() => {
    return bleDevice.readCharacteristicForService(
      SERVICE_UUID,
      CHARACTERISTIC_UUID_BATTERY
    )
  })
}

export function sendCommand(
  config = {
    vMode: 1,
    vSpeed: 1,
    vTimer: 15,
    command: 1
  },
  type
) {
  console.log("send command", `#;${Object.values(config).join()};$`)

  if (type === "mode") {
    const { vMode, vSpeed } = config
    return send(
      base64.encode(`#;${Object.values({ vMode, vSpeed }).join(";")};$`)
    )
  }

  return send(base64.encode(`#;${Object.values(config).join()};$`))
}
