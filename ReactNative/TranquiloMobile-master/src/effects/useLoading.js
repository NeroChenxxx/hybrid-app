import React from "react"

import { LoadingIndicator } from "@components"

export const useLoading = Component => props =>
  props.bleState.scanning ? <LoadingIndicator /> : <Component {...props} />
