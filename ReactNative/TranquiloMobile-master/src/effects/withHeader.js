import React from "react"
import { StatusBar } from "react-native"

import { HamburgerMenu } from "@components"
import { StyledCenterContainer } from "@styles"

export const WithHeader = Component => props => {
  return (
    <StyledCenterContainer>
      <StatusBar hidden={true} />
      <HamburgerMenu />
      <Component {...props} />
    </StyledCenterContainer>
  )
}
