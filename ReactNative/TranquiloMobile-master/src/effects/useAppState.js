import React from "react"
import { AppState } from "react-native"

class UseAppState extends React.Component {
  componentDidMount() {
    AppState.addEventListener("change", this._handleAppStateChange)
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange)
  }

  _handleAppStateChange = async nextAppState => {
    //App has come to the foreground!
    if (
      this.props.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      console.log("active")
      // const res = await readBattery()
      // console.log('_handleAppStateChange', res)
    }

    //App has come to the background!
    if (
      this.props.appState.match(/inactive|active/) &&
      nextAppState === "background"
    ) {
      console.log("background")
    }

    this.props.handleChange("appState", nextAppState)
  }

  render() {
    return this.props.children
  }
}

export const useAppState = Component => props => (
  <UseAppState
    appState={props.appState}
    handleChange={props.handleChange}
    blePair={props.blePair}
  >
    <Component {...props} />
  </UseAppState>
)
