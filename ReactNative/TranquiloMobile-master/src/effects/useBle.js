import React from "react"
import base64 from "react-native-base64"

import { scanAndConnect, notify, bleManger, readBattery, connectToDevice, bleDevice, disconnect } from "@ble"
import {
  MAT_RES_START,
  MAT_RES_STOP,
  MAT_RES_TIME,
  WARN_BLE_OFF,
  CHARACTERISTIC_UUID,
  CHARACTERISTIC_UUID_STATUS,
  CHARACTERISTIC_UUID_BATTERY
} from "@constants"
import { Warning } from "@components"
import { convertBatteryNumToPercent } from "@utils"

class UseBle extends React.Component {
  bleStateListener = null
  bleDisconnectListener = null
  bleNotifyListener = null
  bleNotifyStatusListener = null
  bleNotifyBatteryListener = null

  clearListeners = listeners => {
    listeners.forEach(listener => {
      if (listener) {
        listener.remove()
        listener = null
      }
    })
  }

  setUpDevice = async () => {
    //Read battery once from the mat by the time the app connect to it through bluetooth
    try {
      const { value } = await readBattery()
      const [, batteryLevel] = /#;Batt:(\d+);\$/.exec(
        base64.decode(value)
      )

      this.props.handleChange(
        "batteryLevel",
        convertBatteryNumToPercent(batteryLevel)
      )
    } catch (error) {
      console.error("readBattery", JSON.stringify(error))
    }

    //Listen to the working status of the mat
    this.bleNotifyListener = notify((err, char) => {
      if (!char) return

      const [, command, info] = /#;(\w+)[,:]?(.*);\$/.exec(
        base64.decode(char.value)
      )

      console.log("Notify", command, info)

      switch (command) {
        case MAT_RES_START:
          this.props.handleChange("hasMatStarted", true)
          break
        case MAT_RES_STOP:
          this.props.handleMultiChange({
            hasMatStarted: false,
            curTimer: 0
          })
          break
        default:
          break
      }
    }, CHARACTERISTIC_UUID)

    //Receive a response including timer from the mat every second
    this.bleNotifyStatusListener = notify((err, char) => {
      if (!char) return

      const [, status] = /#;(.*);\$/.exec(base64.decode(char.value))

      const statusArr = status.split(",")

      console.log("Notify", statusArr)

      this.props.handleMultiChange({
        hasMatStarted: statusArr[3] === "1" ? true : false,
        curTimer: this.props.vTimer * 60 - statusArr[4]
      })
    }, CHARACTERISTIC_UUID_STATUS)

    //Recieve the status of battery of the mat at a repated interval
    this.bleNotifyBatteryListener = notify((err, char) => {
      if (!char) return

      const [, batteryLevel] = /#;Batt:(\d+);\$/.exec(
        base64.decode(char.value)
      )

      this.props.handleChange(
        "batteryLevel",
        convertBatteryNumToPercent(batteryLevel)
      )
    }, CHARACTERISTIC_UUID_BATTERY)

    //listen for disconnection event
    this.bleDisconnectListener = bleDevice.onDisconnected(() => {
      console.log("bleDisconnectListener  disconnecting.....")

      //solve android ble connection issue by adding a delay: https://stackoverflow.com/questions/39646253/android-stops-finding-ble-devices-onclientregistered-status-133-clientif-0
      setTimeout(() => {
        this.props.handleMultiChange({
          bleState: {
            scanning: false,
            connected: false
          },
          batteryLevel: 0
        })
      }, 1000)

      // //auto connect after disconnecting
      // connectToDevice(bleDevice) 
      //   .then(() => {
      //     this.props.handleChange("bleState", {
      //       scanning: false,
      //       connected: true
      //     })
      //   })
      //   .then(() => {
      //     this.setUpDevice()
      //   })
      //   .catch(error => console.log(error))
    })
  }

  blePair = () => {
    let intervalId = null

    this.clearListeners([
      this.bleStateListener,
      this.bleDisconnectListener,
      this.bleNotifyListener,
      this.bleNotifyStatusListener,
      this.bleNotifyBatteryListener
    ])

    this.bleStateListener = bleManger.onStateChange(async state => {
      if (state === "PoweredOn") {
        intervalId && clearInterval(intervalId)

        try {
          await scanAndConnect()

          this.clearListeners([this.bleStateListener])

          this.setUpDevice()

          this.props.handleChange("bleState", {
            scanning: false,
            connected: true
          })
        } catch (error) {          
          setTimeout(() => {
            this.props.handleMultiChange({
              bleState: {
                scanning: false,
                connected: false
              },
              batteryLevel: 0
            })
          }, 1000)
        }
      } else if (state == "PoweredOff") {
        Warning("Warning", WARN_BLE_OFF)

        intervalId = setInterval(() => {
          Warning("Warning", WARN_BLE_OFF)
        }, 10000)
      }
    }, true)
  }

  componentDidMount() {
    this.blePair()
  }

  componentWillUnmount() {
    console.log("ble componentWillUnmount")

    this.clearListeners([
      this.bleStateListener,
      this.bleDisconnectListener,
      this.bleNotifyListener,
      this.bleNotifyStatusListener,
      this.bleNotifyBatteryListener
    ])

    bleManger && bleManger.destroy()
  }

  render() {
    return React.cloneElement(this.props.children, { blePair: this.blePair })
  }
}

export const useBle = Component => props => (
  <UseBle
    handleChange={props.handleChange}
    handleMultiChange={props.handleMultiChange}
    vTimer={props.vTimer}
  >
    <Component {...props} />
  </UseBle>
)
