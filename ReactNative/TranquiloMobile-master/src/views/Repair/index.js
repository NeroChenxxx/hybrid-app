import React from "react"
import styled from "styled-components"

import { Header, TextButon } from "@components"
import { WARN_BLE_DISCONNECT } from "@constants"
import { Theme, StyledSmallMargin } from "@styles"

const StyledContainer = styled.View`
  flex: 1;
  align-items: center;
`

export default (Repair = ({ blePair }) => (
  <StyledContainer>
    <StyledSmallMargin />
    <Header size={Theme.headerH3Size} bold align="left">
      {`Oops, your phone is having problems connecting to the mat.`}
    </Header>
    <Header size={Theme.headerH4Size} align="left">
      {WARN_BLE_DISCONNECT}
    </Header>
    <StyledSmallMargin />
    <TextButon
      {...Theme.nomralBtn(0.4, 0.15)}
      blePair={blePair}
      content="Connect"
    />
  </StyledContainer>
))
