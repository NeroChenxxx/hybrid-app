import React from "react"
import { AppState, BackHandler } from "react-native"

import Main from "./Main"
import {
  VIBRATION_MODE_CONSTANT,
  VIBRATION_SPEED_ONE,
  VIBRATION_TIMER_HALF_DAY
} from "@constants"

class Tranquilo extends React.Component {
  state = {
    appState: AppState.currentState,
    bleState: {
      scanning: true,
      connected: false
    },
    hasMatStarted: false,
    vMode: VIBRATION_MODE_CONSTANT,
    vSpeed: VIBRATION_SPEED_ONE,
    vTimer: VIBRATION_TIMER_HALF_DAY, //minutes
    curTimer: 0, //seconds
    batteryLevel: 0
  }

  handleMultiChange = obj =>
    this.setState({
      ...this.state,
      ...obj
    })

  handleChange = (type, value) =>
    this.setState({
      ...this.state,
      [type]:
        typeof value !== "object"
          ? value
          : {
              ...this.state[type],
              ...value
            }
    })

  handleBackButton = () => {
    return true
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton)
  }

  render() {
    return (
      <Main
        {...this.state}
        handleChange={this.handleChange}
        handleMultiChange={this.handleMultiChange}
      />
    )
  }
}

export default Tranquilo
