import React, { Fragment } from "react"
import styled from "styled-components"
import {
  Image,
  View,
  TouchableWithoutFeedback
} from "react-native"

import {
  Header,
  VSegment,
  HSegment,
  RoundButton,
  Button,
  IconButton
} from "@components"
import { Theme } from "@styles"
import { useBle, useLoading, useAppState, WithHeader } from "@effects"
import {
  VIBRATION_MODES,
  VIBRATION_SPEEDS,
  VIBRATION_TIMERS,
  VIBRATION_MODE_CONSTANT,
  VIBRATION_MODE_HEARTBEAT,
  SEGMENT_VMODE_TITLE,
  SEGMENT_VSPEED_TITLE,
  SEGMENT_VTIMER_TITLE,
  SEGMENT_VMODE_BUTTONS,
  SEGMENT_VSPEED_BUTTIONS,
  SEGMENT_VTIMER_BUTTONS
} from "@constants"
import { sendCommand } from "@ble"
import Timer from "./Timer"
import Repair from "./Repair"
import images from "../../assets"

const StyledContentContainer = styled.View`
  width: 100%;
  height: 20%;
  justify-content: center;
`

const Main = props => {
  const {
    blePair,
    handleChange,
    hasMatStarted,
    vMode,
    vSpeed,
    vTimer,
    curTimer,
    bleState: { scanning, connected },
    batteryLevel
  } = props

  const config = {
    vMode,
    vSpeed,
    vTimer,
    command: hasMatStarted ? 0 : 1
  }

  handlePress = async (type, content) => {
    if (type === SEGMENT_VMODE_BUTTONS) {
      //map vibration mode string to mode number
      content =
        content === VIBRATION_MODES[VIBRATION_MODE_CONSTANT]
          ? VIBRATION_MODE_CONSTANT
          : VIBRATION_MODE_HEARTBEAT
    }

    if (hasMatStarted) {
      if (config[type] === content) return

      //change configuration during operation
      config[type] = content
      config["command"] = 1

      type === SEGMENT_VTIMER_BUTTONS
        ? sendCommand(config)
        : sendCommand(config, "mode")
    }

    handleChange(type, content)
  }

  let content =
    !scanning && !connected ? ( //try to reconect when ble disconnected
      <Repair
        blePair={() => {
          blePair()

          handleChange("bleState", {
            scanning: true,
            connected: false
          })
        }}
      />
    ) : (
      <Fragment>
        <StyledContentContainer>
          <VSegment
            title={SEGMENT_VMODE_TITLE}
            bgColor={Theme.brandLightGray}
            render={() =>
              Object.entries(VIBRATION_MODES).map(([key, content]) => (
                <Button
                  {...Theme.nomralBtn(0.45, 0.15)}
                  key={key}
                  content={content}
                  selected={vMode === key}
                  handlePress={() =>
                    handlePress(SEGMENT_VMODE_BUTTONS, content)
                  }
                />
              ))
            }
          />
        </StyledContentContainer>
        <StyledContentContainer>
          <VSegment
            title={SEGMENT_VSPEED_TITLE}
            render={() =>
              VIBRATION_SPEEDS.map((content, i) => (
                <RoundButton
                  {...Theme.roundBtn(0.2)}
                  key={i}
                  content={content}
                  selected={vSpeed === content}
                  handlePress={() =>
                    handlePress(SEGMENT_VSPEED_BUTTIONS, content)
                  }
                />
              ))
            }
          />
        </StyledContentContainer>
        <StyledContentContainer>
          <VSegment
            title={SEGMENT_VTIMER_TITLE}
            bgColor={Theme.brandLightGray}
            render={() =>
              VIBRATION_TIMERS.map((content, i) => (
                <RoundButton
                  {...Theme.roundBtn(0.16)}
                  key={i}
                  content={
                    content === 720 ? (
                      <IconButton
                        name="md-infinite"
                        size={Theme.iconBtn(0.14)}
                      />
                    ) : (
                      content
                    )
                  }
                  selected={vTimer === content}
                  handlePress={() =>
                    handlePress(SEGMENT_VTIMER_BUTTONS, content)
                  }
                />
              ))
            }
          />
        </StyledContentContainer>
        <StyledContentContainer>
          <HSegment
            bgColor={Theme.brandLightGreen}
            render={() => (
              <Fragment>
                {batteryLevel ? (
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <View style={Theme.batterIndicator(0.1)}>
                      <Image
                        source={
                          images.batteryIndicators[
                            Math.trunc(batteryLevel / 10)
                          ]
                        }
                        style={{ width: "100%" }}
                        resizeMode="contain"
                      />
                    </View>
                    <Header size={20} bold>
                      {` ${batteryLevel}%`}
                    </Header>
                  </View>
                ) : null}
                <TouchableWithoutFeedback
                  onPress={() => {
                    sendCommand(config)
                  }}
                >
                  <View style={Theme.roundBtn(0.2)}>
                    <Image
                      source={hasMatStarted ? images.stop : images.start}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="cover"
                    />
                  </View>
                </TouchableWithoutFeedback>
                {vTimer <= 60 &&
                curTimer <= 60 * 60 && ( //no show timer above 60 minutes
                    <Timer
                      hasMatStarted={hasMatStarted}
                      timer={curTimer || vTimer * 60}
                      handleChange={handleChange}
                    />
                  )}
              </Fragment>
            )}
          />
        </StyledContentContainer>
      </Fragment>
    )

  return (
    <>
      <View
        style={{
          width: "100%",
          height: "20%",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Image
          source={images.logo}
          style={{ width: "80%", height: "55%" }}
          resizeMode="contain"
        />
      </View>
      {content}
    </>
  )
}

export default useBle(useAppState(useLoading(WithHeader(Main))))
