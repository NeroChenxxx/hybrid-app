import React from "react"
import PropTypes from "prop-types"

import { Header } from "@components"
import { Theme } from "@styles"
import { convertNumToTime } from "@utils"
import { VIBRATION_TIMER_HALF_DAY } from "@constants"

export default class Timer extends React.Component {
  static propTypes = {
    hasMatStarted: PropTypes.bool.isRequired,
    timer: PropTypes.number.isRequired,
    handleChange: PropTypes.func.isRequired
  }

  state = {
    remaining: VIBRATION_TIMER_HALF_DAY * 60
  }

  componentDidMount() {
    const { timer } = this.props

    this.setState({
      remaining: timer //seconds
    })
  }

  componentWillUnmount() {
    this.stopTimer()
  }

  componentDidUpdate(prevProps) {
    const { timer, hasMatStarted } = this.props

    // if (hasMatStarted === true && prevProps.hasMatStarted === false) {
    //   this.startTimer()
    // }

    // if (hasMatStarted === false && prevProps.hasMatStarted === true) {
    //   this.stopTimer()
    // }

    if (timer !== prevProps.timer) {
      this.setState({
        remaining: timer
      })
    }
  }

  startTimer = () => {
    this.intervalId = setInterval(() => {
      const { remaining } = this.state

      if (remaining === 1) return this.stopTimer()

      this.setState({ ...this.state, remaining: remaining - 1 })
    }, 1000)
  }

  stopTimer = () => {
    this.intervalId && clearInterval(this.intervalId)
    this.intervalId = null
  }

  render() {
    const { remaining } = this.state

    return (
      <Header size={Theme.headerH1Size}>{convertNumToTime(remaining)}</Header>
    )
  }
}
