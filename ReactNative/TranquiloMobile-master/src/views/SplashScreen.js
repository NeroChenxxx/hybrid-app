import React, { useEffect } from 'react'

import {Header} from '@components';
import {StyledCenterContainer, Theme} from '@styles';
import { registerForCoarseLocationAsync } from "@ble"
import { Navigator } from "../navigation"

const requestBleLocation = async () => {
    const status = await registerForCoarseLocationAsync()

    if(!status) {
        requestBleLocation()
    } else{
      Navigator.navigate("App")
    }
}

const SplashScreen = () => {
  useEffect(() => {
    requestBleLocation()
  }, [])

  return (
    <StyledCenterContainer>
      <Header size={Theme.headerH1Size} bold>
        Tranquilo
      </Header>
    </StyledCenterContainer>
  );
};

export default SplashScreen;
