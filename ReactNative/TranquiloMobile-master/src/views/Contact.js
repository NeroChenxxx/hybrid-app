import React from "react"
import { Linking } from "react-native"

import { WithHeader } from "../effects"
import { Theme, StyledSmallMargin } from "@styles"
import { Header, Button } from "@components"
import { CUSTOMER_SUPPORT_EMAIL } from "@constants"

const Contact = props => {
  return (
    <>
      <Header size={Theme.headerH1Size} bold>
        Contact
      </Header>
      <StyledSmallMargin />
      <Button
        {...Theme.nomralBtn(0.4, 0.15)}
        content={"Email Us"}
        handlePress={() =>
          Linking.openURL(
            `mailto:${CUSTOMER_SUPPORT_EMAIL}?subject=Tranquilo%20mat%20Inquiry%20from%20App&body=Hi.%20I%E2%80%99m%20using%20the%20Tranquilo%20mat%20and%20have%20a%20question%20or%20problem.%20Can%20you%20contact%20me%20to%20help%3F%20Thanks.%0D%0A%0D%0ASincerely`
          )
        }
      />
    </>
  )
}

export default WithHeader(Contact)
