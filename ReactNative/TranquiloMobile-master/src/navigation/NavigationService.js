import {
  NavigationActions,
  StackActions
} from "react-navigation"

import { DrawerActions } from 'react-navigation-drawer'

class NavigationService {
  _navigator = null

  setTopLevelNavigator = navigatorRef => {
    this._navigator = navigatorRef
  }

  navigate = (routeName, params) => {
    this._navigator.dispatch(
      NavigationActions.navigate({
        routeName,
        params
      })
    )
  }

  pop = () => {
    this._navigator.dispatch(StackActions.pop())
  }

  goBack = () => {
    this._navigator.dispatch(NavigationActions.back())
  }

  openDrawer = () => {
    this._navigator.dispatch(DrawerActions.openDrawer())
  }
}

export const Navigator = new NavigationService()
