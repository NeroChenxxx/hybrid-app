import 'react-native-gesture-handler' //https://github.com/kmagiera/react-native-gesture-handler/issues/320#issuecomment-537945905
import { createAppContainer } from "react-navigation"
import { createDrawerNavigator } from 'react-navigation-drawer'
import { createStackNavigator } from 'react-navigation-stack'
import { Platform } from "react-native"

import Tranquilo from "../views"
import Contact from "../views/Contact"
import SplashScreen from "../views/SplashScreen"

const AppNavigator = createDrawerNavigator( {
  Home: Tranquilo,
  Contact: Contact
}, {
  initialRouteName: "Home",
  contentOptions: {
    labelStyle: {
      fontFamily: "AllRoundGothicW01-Book"
    }
  }
})

const AppStackNavigator = createStackNavigator({
  SplashScreen,
  App: AppNavigator
}, {
  initialRouteName: "SplashScreen",
  headerMode: "none"
})

export default Platform.OS === "ios" ? createAppContainer(AppNavigator): createAppContainer(AppStackNavigator)

export * from "./NavigationService"
