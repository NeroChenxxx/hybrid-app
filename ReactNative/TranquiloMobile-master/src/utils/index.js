export function convertNumToTime(num) {
  if (typeof num !== "number") return

  function appendZero(num) {
    return num < 10 ? `0${num}` : num
  }

  const min = appendZero(Math.trunc(num / 60))
  const sec = appendZero(num % 60)

  if (min && sec) {
    return `${min}:${sec}`
  } else if (min && !sec) {
    return `${min}:00`
  } else if (!min && sec) {
    return `00:${sec}`
  }
}

export function convertBatteryNumToPercent(num) {
  const level = Math.min(1000, Math.max(400, num))

  return Math.floor(((level - 400) / 600) * 100)
}
