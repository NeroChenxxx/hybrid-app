import React from "react"
import { Alert } from "react-native"

export const Warning = (title, error, handler = () => {}) => {
  const errorMsg = typeof error === "object" ? error.message : error

  return Alert.alert(title, errorMsg, [{ text: "OK", onPress: handler }])
}
