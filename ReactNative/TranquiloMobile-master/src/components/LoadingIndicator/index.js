import React from "react"
import styled from "styled-components"

import { StyledCenterContainer, Theme } from "@styles"
import { Header } from "@components"

const StyledActivityIndicator = styled.ActivityIndicator`
  color: ${props => props.theme.brandPrimary};
`

export const LoadingIndicator = () => (
  <StyledCenterContainer>
    <StyledActivityIndicator size="large" />
    <Header size={Theme.headerH2Size} bold>
      Pairing....
    </Header>
  </StyledCenterContainer>
)
