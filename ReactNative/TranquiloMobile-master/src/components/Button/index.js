import React from "react"
import styled from "styled-components"
import PropTypes from "prop-types"
import Icon from 'react-native-vector-icons/Ionicons';

import { Theme } from "@styles"

const StyledButton = styled.TouchableOpacity`
  border-radius: ${props => props.borderRadius};
  background-color: ${props =>
    props.bgColor
      ? props.bgColor
      : props.selected
      ? props.theme.btnSelectedBgColor
      : props.theme.btnNormalBgColor};
  width: ${props => props.width};
  height: ${props => props.height};
  justify-content: center;
  align-items: center;
  border-color: ${props => props.theme.brandDark};
  border-width: 1;
`

const StyledBtnText = styled.Text`
  font-family: ${props => props.theme.btnFontFamily};
  font-size: ${props => props.theme.btnTextSizeLarge};
  color: ${props =>
    props.selected ? props.theme.btnSelectedColor : props.theme.btnNormalColor};
`

//Normal Text Button
export const TextButon = props => (
  <StyledButton onPress={props.blePair} {...props}>
    {typeof props.content === "string" ? (
      <StyledBtnText>{props.content}</StyledBtnText>
    ) : (
      props.content
    )}
  </StyledButton>
)

//Ble Button
export const RoundButton = ({
  width,
  height,
  borderRadius = width,
  selected = false,
  content,
  handlePress,
  disabled = false,
  bgColor
}) => (
  <StyledButton
    width={width}
    height={height}
    borderRadius={borderRadius}
    selected={selected}
    disabled={disabled}
    onPress={handlePress}
    bgColor={bgColor}
  >
    <StyledBtnText selected={selected}>{content}</StyledBtnText>
  </StyledButton>
)

RoundButton.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  raidus: PropTypes.number,
  handleChange: PropTypes.func,
  selected: PropTypes.bool,
  content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object
  ])
}

export const Button = props => <RoundButton {...props} />

//Icon Button
export const IconButton = ({ onPress, ...props }) => (
  <Icon color={Theme.brandBlack} {...props} onPress={onPress} />
)

export const ToggleButton = ({ hasMatStarted, size }) => (
  <IconButton
    size={size}
    name={hasMatStarted ? "md-pause" : "md-play"}
    color={Theme.brandWhite}
  />
)

ToggleButton.propTypes = {
  hasMatStarted: PropTypes.bool.isRequired
}
