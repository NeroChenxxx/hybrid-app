import React from "react"
import styled from "styled-components"
import PropTypes from "prop-types"

import { Header } from "../Header"
import { StyledHCenterContainer } from "@styles"
import { Theme } from "@styles"

const propTypes = {
  title: PropTypes.string,
  render: PropTypes.func.isRequired,
  bgColor: PropTypes.string
}

const StyledVSegment = styled.View`
  flex-grow: 1;
  background-color: ${props => props.bgColor || props.theme.brandWhite};
`

const StyledVButtonGroups = styled(StyledHCenterContainer)`
  flex-grow: 8;
  justify-content: space-evenly;
  align-items: center;
`

const StyledVTitleContainer = styled.View`
  flex-grow: 1;
  justify-content: center;
`

export const VSegment = ({ title, bgColor, render }) => (
  <StyledVSegment bgColor={bgColor}>
    <StyledVTitleContainer>
      {title && <Header size={Theme.headerH2Size}>{title}</Header>}
    </StyledVTitleContainer>
    <StyledVButtonGroups>{render()}</StyledVButtonGroups>
  </StyledVSegment>
)

VSegment.propTypes = propTypes

const StyledHSegment = styled(StyledVSegment)`
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
`

export const HSegment = ({ bgColor, render }) => (
  <StyledHSegment bgColor={bgColor}>{render()}</StyledHSegment>
)

HSegment.propTypes = propTypes
