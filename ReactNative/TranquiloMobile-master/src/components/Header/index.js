import React from "react"
import styled from "styled-components"
import PropTypes from "prop-types"

const StyledHeader = styled.Text`
  font-family: ${props => props.fontFamily || props.theme.titleFontfamily};
  font-size: ${props => props.size || props.theme.titleFontSize};
  font-weight: ${props => (props.bold ? "bold" : "normal")};
  text-align: ${props => props.align || "center"};
`

export const Header = ({ size, children, bold, align, onPress }) => (
  <StyledHeader size={size} bold={bold} align={align} onPress={onPress}>
    {children}
  </StyledHeader>
)

Header.propTypes = {
  size: PropTypes.number,
  bold: PropTypes.bool,
  children: PropTypes.node.isRequired
}
