import React from "react"
import Icon from 'react-native-vector-icons/Entypo';
import { View } from "react-native"

import styles from "./styles"
import { Navigator } from "../../navigation"

export const HamburgerMenu = ({ ...props }) => (
  <View style={styles.container}>
    <Icon
      name="menu"
      size={45}
      iconStyle={styles.icon}
      onPress={Navigator.openDrawer}
    />
  </View>
)
