import { StyleSheet } from "react-native"
import { Theme } from "@styles/"

export default StyleSheet.create({
  icon: {
    color: Theme.brandPrimary
  },
  container: {
    position: "absolute",
    zIndex: 10,
    top: 10,
    left: 15
  }
})
