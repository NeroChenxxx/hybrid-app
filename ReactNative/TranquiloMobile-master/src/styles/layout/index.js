import styled from "styled-components"

export const StyledFooter = styled.SafeAreaView`
  position: absolute;
  bottom: 30;
`

export const StyledContainer = styled.View`
  flex: 1;
`

export const StyledCenterContainer = styled(StyledContainer)`
  align-items: center;
  justify-content: center;
  background-color: white;
`

export const StyledHCenterContainer = styled(StyledContainer)`
  flex-direction: row;
  justify-content: center;
`

export const StyledSparseContainer = styled(StyledContainer)`
  align-items: center;
  justify-content: space-around;
`
