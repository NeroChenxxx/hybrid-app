import styled from "styled-components";

export const StyledSmallMargin = styled.View`
  margin: 10px;
`;
