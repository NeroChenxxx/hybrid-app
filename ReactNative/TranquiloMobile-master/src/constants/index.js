export const SERVICE_UUID = "f3641400-00b0-4240-ba50-05ca45bf8abc"
export const CHARACTERISTIC_UUID = "f3641401-00b0-4240-ba50-05ca45bf8abc"
export const CHARACTERISTIC_UUID_STATUS = "f3641402-00b0-4240-ba50-05ca45bf8abc"
export const CHARACTERISTIC_UUID_BATTERY =
  "f3641403-00b0-4240-ba50-05ca45bf8abc"

export const MAT_RES_START = "start"
export const MAT_RES_STOP = "stop"
export const MAT_RES_TIME = "TIME"

export const LISTENER_NOTIFY = "LISTENER_NOTIFY"
export const LISTENER_DISCONNECT = "LISTENER_DISCONNECT"
export const LISTENER_DEVICE_BLE_STATE = "LISTENER_DEVICE_BLE_STATE"

export const BLE_CONNECTED = "BLE_CONNECTED"
export const BLE_DISCONNECTED = "BLE_DISCONNECTED"

//warning
export const WARN_BLE_OFF = "Turn on bluetooth on your phone!"
export const WARN_BLE_DISCONNECT = `
To reconnect, please:

  • Move closer to the mat

  • Make sure your phone's Bluetooth is turned on 
    
  • Make sure the mat's batteries are working

Click “Connect” below to try pairing again.
`

//config
export const VIBRATION_MODE_CONSTANT = "1"
export const VIBRATION_MODE_HEARTBEAT = "2"
export const VIBRATION_MODES = {
  [VIBRATION_MODE_CONSTANT]: "vibration", //constant
  [VIBRATION_MODE_HEARTBEAT]: "heartbeat"
}

export const VIBRATION_SPEED_ONE = 1
export const VIBRATION_SPEED_TWO = 2
export const VIBRATION_SPEED_THREE = 3
export const VIBRATION_SPEED_FOUR = 4
export const VIBRATION_SPEEDS = [
  VIBRATION_SPEED_ONE,
  VIBRATION_SPEED_TWO,
  VIBRATION_SPEED_THREE,
  VIBRATION_SPEED_FOUR
]

export const VIBRATION_TIMER_QUARTER = 15
export const VIBRATION_TIMER_HALF_HOUR = 30
export const VIBRATION_TIMER_THREE_QUARTER = 45
export const VIBRATION_TIMER_HOUR = 60
export const VIBRATION_TIMER_HALF_DAY = 720
export const VIBRATION_TIMERS = [
  VIBRATION_TIMER_QUARTER,
  VIBRATION_TIMER_HALF_HOUR,
  VIBRATION_TIMER_THREE_QUARTER,
  VIBRATION_TIMER_HOUR,
  VIBRATION_TIMER_HALF_DAY
]

//segment
export const SEGMENT_VMODE_TITLE = "mode"
export const SEGMENT_VSPEED_TITLE = "vibration speed"
export const SEGMENT_VTIMER_TITLE = "timer - minutes"

//the followings should be equal to the name property of react state
export const SEGMENT_VMODE_BUTTONS = "vMode"
export const SEGMENT_VSPEED_BUTTIONS = "vSpeed"
export const SEGMENT_VTIMER_BUTTONS = "vTimer"

//Customer support
export const CUSTOMER_SUPPORT_PHONE_NUMBER = "1-888-396-6552"
export const CUSTOMER_SUPPORT_EMAIL = "tranquilobabycs@beteshgroup.com"
