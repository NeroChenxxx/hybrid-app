void main()
{
{
  /*************** 判断类型 ********************/
  var i = "strstr";
  if(i is String)
  {
    print("i is String.");
  }

  /****************String********************/
  String str = 'chenlihui';
  String str2 = "chenlihui";
  String str3 = """chen
  li
  hui""";
  print(str3);
  /****************List********************/
  //List <String>表示数组内必须是String，不指定的话可以是任意类型。
   var lt1 = <String>["ChenLiHui", "WuChuanZhi", "ChenYiYi"];
   //追加
   lt1.add("who1"); 
   lt1.addAll(["who2","who3"]);
   //删除
   lt1.remove("ChenLiHui");
   //指定位置添加
   lt1.insert(0, "ChenLiHui");
   //查找,查找不到返回-1，查找到选择下标
   lt1.indexOf("xx");
   print("List1 Len =  "+ lt1.length.toString());
   print("List1 =  $lt1");
   print("List1 = " + "$lt1");

  //list转换为字符串
  List lt4 = ["vstr",10,10.2,true];
  String lstr = lt4.join('-');
  print("List to String:"+lstr);
  List lt5 = lstr.split("-");
  print("String to List:"+lt5.toString());
}
  //固定长度数组，不能增加长度,不能改变类型
   var lt2 = List.filled(10, "");
   print("List2 =  $lt2");
   lt2[0] = "Chenlihui";
   print("List2 =  $lt2");

   var lt3 = List<int>.filled(10, 1);
   for(int i=0; i < 10; i++)
   {
     lt3[i] = i;
   }
   print("List3 =  $lt3");

   if(lt3.isNotEmpty)
   {
     print(lt3.reversed);
     print(lt3);
     //翻转数组方法
     lt3 = lt3.reversed.toList();
     print(lt3);
   }
   /**************** Set ********************/
  print("Set 就是不能有重复数据的List,不能通过下标获取值");
  Set st1 = {"chen", "li", "chen"};
  print(st1);
  print(st1.toList());

   /**************** Map ********************/
   var mp1 = {
     "a":"one",
     "b":"two",
     "c":"three",
     "d":"four",
   };
   print(mp1);
   //打印所有key
   print(mp1.keys.toList());
   //打印所有value
   print(mp1.values.toList());
   //查看映射内是否包含此value
   print(mp1.containsValue("one"));

   var mp2 = new Map();
   mp2["name"] = "张三";
   print(mp2);
}