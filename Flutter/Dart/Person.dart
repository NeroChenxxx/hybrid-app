class Person
{
  //属性
  String name = "默认";
  int age = 0;
  //内部属性,外部无法调用，类要单独一个文件，命名方法为_开头
  String _sex="不详";
  bool _child = false;
  static int shownum = 0;

  //构造函数，只能有一个
  Person(String name, int age, String sex)
  {
    this.name = name;
    this.age = age;
    this._sex = sex;
  }

  //构造函数赋初值，简写
  //Person(this.name,this.age,this._sex);

  //构造函数赋初值方法,初始化列表
  // Person():name="",age=0{

  // }

  //命名构造函数
  Person.showAge()
  {
    print('命名构造函数');
  }

  //方法
  void printInfo()
  {
    //${}可显示函数和多级变量
    print("name: ${this.name}  age: ${this.age} sex: ${this._sex}");
    this._printEnd();
  }

  //内部方法
  void _printEnd()
  {
    print("--print end--");
  }

  //geter，计算属性,外部调用时可以直接访问此属性

  get month{
    return this.age*12;
  }

  set isChild(value)
  {
    this._child = value;
  }
  get isChild{
    return this._child;
  }

  //静态方法,静态方法只能访问静态成员，非静态方法可以访问任意属性
  static void show()
  {
    shownum = 0;
    print("this is static fun");
  }

}