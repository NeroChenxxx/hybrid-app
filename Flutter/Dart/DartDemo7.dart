import 'MySqlExtern.dart';
import 'Db.dart';
  /*面向对象: 封装、继承、多态
  
  Dart中的抽象类: 子类可以继承抽象类，也可以实现接口，抽象类的作用是约束主类。
  1、 抽象类用abstract声明，类内没有实现的方法是抽象方法。
  2、 抽象类被继承时需要实现所有抽象方法。抽象类被引用为接口时，必须实现所有属性和方法
  3、 抽象类不能被实例化，只有继承他的子类可以

  Dart中的接口: 
  1、 普通类和抽象类都可以作为接口被实现。
  2、 普通类接口继承后所有属性和方法都要覆写，所以一般直接使用抽象类当做接口

  externs和implements的区别：
  1、 既要实现接口又要使用抽象类本身的方法时使用externs
  2、 只为了约束子类，制定标准时，使用implements


  */
void main(List<String> args) {
  
  MySqlExtern mysql = MySqlExtern("192.168.1.1");
  mysql.printInfo();
  Map p = {"a":"one",};
  mysql.add(p);
  mysql.dbDestory();

  Db db = MySqlExtern("192.168.1.2");
  db.printInfo();
  db.add(p);
  db.dbDestory();

}