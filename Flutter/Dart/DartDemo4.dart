void main(List<String> args) {
  print("=======可选参数=======");
  fun1("陈立辉","男",32);
  fun1("陈立辉");
  fun1("陈立辉",);

  print("=======命令参数=======");
  fun1("陈立辉","男",32);
  fun2("陈立辉");
  fun2("陈立辉",age:33);

  print("=======函数指针=======");
  fun4(fun3);

  print("=======匿名函数=======");
  var fun5 = (){
    print("this is nuname.");
  };
  fun4(fun5);

  print("=======局部函数=======");
  fun6()
  {
    print("this is local.");
  }
  fun6();

  print("=======自执行函数=======");
  //()(),在第一个()中加入匿名函数
((int e){
    print("this is auto run. e=$e");
  }
)(12);
}


//函数定义时的参数[]内为可省略参数，类型需要加上,因为可能为null。
//sex为赋初值参数，不传入实参时为默认值
void fun1(String name, [String? sex = "不详", int? age]){
  print("年龄: $name 年龄: $age 性别: $sex");
}

//命名参数在可选参数的基础上，可以指定形参名来传入实参
void fun2(String name, {String? sex = "不详", int? age}){
  print("年龄: $name 年龄: $age 性别: $sex");
}

//函数指针
void fun3(){
  print("this is fun3");
}
void fun4(fun)
{
  fun();
}

