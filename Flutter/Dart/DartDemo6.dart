import 'Person.dart';
  /*面向对象: 封装、继承、多态
  
  Dart中类的继承:
  1、子类使用externs关键词来继承父类。
  2、子类会继承父类中可见的属性和方法，但不会继承构造函数
  3、子类能复写父类的方法 getter setter


  */

class Woman extends Person{
  //初始化列表执行super
  Woman(String name, int age) : super(name, age, '女'){
  }
  //继承命名构造
  Woman.showAge() : super.showAge(){
  }

  //复写父类函数
  @override
  void printInfo()
  {
    print("this is for woman.");
    super.printInfo();
  }

  void isWoman()
  {
    
  }
}
void main(List<String> args) {

  Woman w1 = Woman("孙二娘", 26);
  w1.printInfo();
}