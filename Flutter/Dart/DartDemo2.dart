void main(List<String> args) {
  {
    print("=========赋值 运算==========");
    var a = 7;
    var b = 5;
    print("a="+a.toString()+", b="+b.toString());
    print("除 a/b = "+(a/b).toString());
    print("取余 a%b = "+(a%b).toString());
    print("取整 a~/b = "+(a~/b).toString());
    var c;
    c ??= 4;
    print("c为null时赋值   c = "+c.toString());
    int d = c??5;
    print("c不为null时赋值   d = "+d.toString());

  }
  {
    print("========= parse ========");
    print("字符串转化为数字");
    String str1 = "10";
    int num1 = int.parse(str1);
    double num2 = double.parse(str1);
    int num3 = int.parse(str1, radix: 16); 
    print("str: "+str1);
    print("->int: "+num1.toString());
    print("->double: "+num2.toString());
    print("->Hex: "+num3.toString());

    print("字符串带小数点时，转换为int会报错，可以先转换成double,在转换为int,转换过程丢掉小数点后");
    String str2 = "12.2";
    print("str2: "+str2);
    var num4;
    //int.parse会报异常，catch下
    try{
      num4= int.parse(str2);
      print("->int: "+num4.toString());
    }catch(err)
    {
      //没初值就赋值，必须是var类型
      num4 ??= 1;
      print("->int err: "+num4.toString());
    }

    int num5 = double.parse(str2).toInt();
    print("->double->int: "+num5.toString());

    print("可以用tryParise 判断是否可转换，防止崩溃");

    if(int.tryParse(str2) != null)
    {
      int num6 = int.parse(str2);
      print(num6);
    }
  
    //int?  能够接收int类型和null
    int? num7 = int.tryParse(str2);
    print(num7);  

  }
}