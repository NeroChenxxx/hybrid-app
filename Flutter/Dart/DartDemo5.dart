import 'Person.dart';
void main(List<String> args) {
  var p1 = Person("chenlihui",32,"man");
  var p2 = Person.showAge();

  p1.printInfo();
  print("month: ${p1.month}");

  p1.isChild = true;
  print("child: ${p1.isChild}");

  Person.show();


  //条件运算符 ?,为空时不运行，防止移仓
  Person? p;
  p?.printInfo();

  //类型判断 is
  var p3 = Person("chenlihui",32,"man");
  if(p3 is Person)
  {
    print("p3 is Person");
  }
  else 
  {
    print("p3 is not Person");
  }

  //类型转换 as
  var str;
  str = "";
  str = Person("xxx",32,"man");
  (str as Person).printInfo();

  //级联操作 ..
  Person p4 = Person("张三",32,"man");
  
  p4..name="李四"
  ..age = 28
  ..printInfo();
}