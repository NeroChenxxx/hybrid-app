void main()
{
 print("list 去重方法");
 var lt1 = ["a","aa","b","aa","a"];
 var st1 = new Set();
 st1.addAll(lt1);
 print(st1.toList());

 print("循环List");
 var lt2 = ["a","b","c","d","e",1,2,3.0];
  List lt3 = [];
 for(int i = 0; i < lt2.length; i++)
 {
   print(lt2[i]);
   lt3.add(lt2[i]);
 }
  print(lt3);

 for(var item in lt2)
 {
   print(item);
 }


 lt2.forEach((element) {
   print("$element");
 });


 print("map函数，返回运算后的新list");
 var lt4 = [1,2,3,4];
 var lt5 = ["1","2","3","4"];
 //只有一行代码时，使用=>
 var lt6 = lt5.map((e) => e*2);
 //多行代码时,传入省略函数名的函数
 var lt8 = lt5.map((e){
   return e*3;
  });
 print(lt6);
 print(lt8);

 print("where函数,返回符合条件的新list");
 var lt7 = lt4.where((element) => element>2);
 print(lt7);

 print("any函数,判断list中是否包含符合条件的");
 print(lt4.any((element) => element==2));
 print("every函数,判断list中是否全部符合条件");
 print(lt4.every((element) => element>=2));
}