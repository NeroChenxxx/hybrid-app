import 'dart:ui';

import 'package:demo1/Dashboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'MianWidget.dart';

void main() {
  runApp(DashBoard());
  //runApp(MainWidget());
  //runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '余额（W）:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}


class DashBoardTablePainter {
 
  final double tableSpace;
  var speedTexts=["0","20","40","60","80","100","120","140","160","180","200","230","260"];
  final Size size;
  final PictureRecorder _recorder = PictureRecorder();
 
  DashBoardTablePainter(this.tableSpace,this.size);
 
  Picture getBackGround() {
    Canvas canvas=Canvas(_recorder);
    canvas.clipRect(new Rect.fromLTWH(0.0, 0.0, size.width, size.height));
    drawTable( canvas,  size);
    return _recorder.endRecording();
  }
 
 
  ///画仪表盘的表格
  void drawTable(Canvas canvas, Size size){
    canvas.save();
    double halfWidth=size.width/2;
    double halfHeight=size.height/2;
    canvas.translate(halfWidth, halfHeight);
 
    Paint paintMain=new Paint();
    paintMain.color=Colors.blue;
    paintMain.strokeWidth=2.5;
    paintMain.style=PaintingStyle.fill;
 
 
    Paint paintOther=new Paint();
    paintOther.color=Colors.blue;
    paintOther.strokeWidth=1;
    paintOther.style=PaintingStyle.fill;
 
    drawLongLine(canvas,paintMain,halfHeight,speedTexts[6]);
 
    canvas.save();
    for(int i=61;i<=120;i++){
      canvas.rotate(tableSpace);
      if(i%10==0){
        int a=(i/10).ceil();
        changePaintColors(paintMain,i);
        drawLongLine(canvas,paintMain,halfHeight,speedTexts[a]);
      }else if(i%5==0){
        changePaintColors(paintMain,i);
        drawMiddleLine(canvas,paintMain,halfHeight);
      }else{
        changePaintColors(paintOther,i);
        drawSmallLine(canvas,paintOther,halfHeight);
      }
    }
    canvas.restore();
 
 
    canvas.save();
    for(int i=59;i>=0;i--){
      canvas.rotate(-tableSpace);
      if(i%10==0){
        int a=(i/10).ceil();
        changePaintColors(paintMain,i);
        drawLongLine(canvas,paintMain,halfHeight,speedTexts[a]);
      }else if(i%5==0){
        changePaintColors(paintMain,i);
        drawMiddleLine(canvas,paintMain,halfHeight);
      }else{
        changePaintColors(paintOther,i);
        drawSmallLine(canvas,paintOther,halfHeight);
      }
    }
    canvas.restore();
 
    canvas.restore();
  }
 
 
  void changePaintColors(Paint paint,int value){
    if(value<=20){
      paint.color=Colors.green;
    }else if(value<80){
      paint.color=Colors.blue;
    }else{
      paint.color=Colors.red;
    }
  }
 
  ///画仪表盘上的长线
  void drawLongLine(Canvas canvas,Paint paintMain,double halfHeight,String text){
    canvas.drawLine(new Offset(0.0, -halfHeight), new Offset(0.0,  -halfHeight+15), paintMain);
 
    TextPainter textPainter = new TextPainter();
    textPainter.textDirection = TextDirection.ltr;
    textPainter.text = new TextSpan(text: text, style: new TextStyle(color:paintMain.color,fontSize: 15.5,));
    textPainter.layout();
    double textStarPositionX = -textPainter.size.width / 2;
    double textStarPositionY = -halfHeight+19;
    textPainter.paint(canvas, new Offset(textStarPositionX, textStarPositionY));
  }
 
 
  void drawMiddleLine(Canvas canvas,Paint paintMain,double halfHeight){
    canvas.drawLine(new Offset(0.0, -halfHeight), new Offset(0.0, -halfHeight+10), paintMain);
  }
 
 
  ///画短线
  void drawSmallLine(Canvas canvas,Paint paintOther,double halfHeight){
    canvas.drawLine(new Offset(0.0, -halfHeight), new Offset(0.0, -halfHeight+7), paintOther);
  }
 
}


class IndicatorPainter {
 
  final PictureRecorder _recorder = PictureRecorder();
  final Size size;
  
  IndicatorPainter(this.size);
 
  ///画速度指针
  Picture drawIndicator(){
    Canvas canvas=Canvas(_recorder);
    canvas.clipRect(new Rect.fromLTWH(0.0, 0.0, size.width, size.height));
    
    double halfHeight=size.height/2;
    double halfWidth=size.width/2;
    Path path=new Path();
    path.moveTo(-2.5, 20);
    path.lineTo(2.5, 20);
    path.lineTo(6.0, -30);
    path.lineTo(0.5, -halfHeight+8);
    path.lineTo(-0.5, -halfHeight+8);
    path.lineTo(-6.0, -30);
    path.close();
    canvas.save();
 
    canvas.translate(halfWidth, halfHeight);
 
    Paint paint=new Paint();
    paint.color=Colors.red;
    paint.style=PaintingStyle.fill;
 
    canvas.drawPath(path, paint);
    
    paint.color=Colors.black;
    canvas.drawCircle(new Offset(0.0,0.0), 6, paint);
 
    canvas.restore();
    return _recorder.endRecording();
  }
}

class DashBoardState extends State<DashBoard>{
 
  final  platform = const MethodChannel('com.flutter.lgyw/sensor');
  bool _isGetPressure=false;
  int pressures=0;final double wholeCirclesRadian=6.283185307179586;
  ///虽然一个圆被分割为160份，但是只显示120份
  final int tableCount=160;
  late Size dashBoardSize;
  late double tableSpace;
  late Picture _pictureBackGround;
  late Picture _pictureIndicator;
 
  @override
  void initState() {
    super.initState();
    dashBoardSize=new Size(300.0,300.0);
    tableSpace=wholeCirclesRadian/tableCount;
    _pictureBackGround=DashBoardTablePainter(tableSpace,dashBoardSize).getBackGround();
    _pictureIndicator=IndicatorPainter(dashBoardSize).drawIndicator();
  }
 
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("汽车仪表盘"),
      ),
      body: new Center(
        child:GestureDetector(
          onPanDown:(DragDownDetails dragDownDetails){
            _isGetPressure=true;
            boostSpeed();
          },
          onPanCancel: (){
            handleEndEvent();
          },
          onPanEnd: (DragEndDetails dragEndDetails){
            handleEndEvent();
          },
          child:new CustomPaint(
            size: dashBoardSize,
            //painter: new DashBoard.IndicatorPainter(pressures,tableSpace,_pictureBackGround,_pictureIndicator),
          ),
        ),
      ),
    );
  }
 
  void boostSpeed() async {
    while (_isGetPressure){
      if(pressures<120){
        setState(() {
          pressures++;
        });
      }
      await Future.delayed(new Duration(milliseconds: 30));
    }
  }
 
 
  void handleEndEvent(){
    _isGetPressure=false;
    bringDownSpeed();
  }
 
 
  void bringDownSpeed() async {
    while (!_isGetPressure){
      setState(() {
        pressures--;
      });
 
      if(pressures<=0){
        break;
      }
      await Future.delayed(new Duration(milliseconds: 30));
    }
  }
}
