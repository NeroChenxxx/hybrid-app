import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class MainWidget extends StatefulWidget {
  MainWidget({Key? key}) : super(key: key);

  @override
  _MainWidgetState createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> {
  @override
  Widget build(BuildContext context) {
    //MaterialApp用来设置整体格式
    return MaterialApp(
      //Scaffold可添加导航栏
      home: Scaffold(
        //导航栏
        appBar: AppBar(
          //AppBar 名称和属性
          title: Text("HelloWorldDemo"),
          backgroundColor: Colors.green,
          
        ),
      body:HomeContent(),
      ),
      theme: ThemeData(
        //MaterialApp 整体色调
        primaryColor: Colors.red
      ),
    );


  }
}


class HomeContent extends StatelessWidget {
  const HomeContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Center(
        child: Container(
          child: Text("HelloWorld",
          textAlign: TextAlign.center,
          ),
          //高宽
          height: 100,
          width: 200,
          //背景颜色
          decoration: BoxDecoration(
            color: Colors.yellow,
            //边框
            border: Border.all(
              color: Colors.purple,
              width: 15
            )
          ),
        ),
      );
  }
}