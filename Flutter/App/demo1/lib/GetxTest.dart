import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:libserialport/libserialport.dart';

//响应式.obs和Obx，搭配的响应式刷新，小空间好用，dropdownbutton不会重新bulid
class GetxHomePage extends StatelessWidget {
  const GetxHomePage({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final Controller ctrl = Get.put(Controller());
    return Scaffold(
      // 使用Obx(()=>每当改变计数时，就更新Text()。
      appBar: AppBar(title: Obx(() => Text("Clicks: ${ctrl.count}"))),
 
      // 用一个简单的Get.to()即可代替Navigator.push那8行，无需上下文！
      body: Obx(()
      {
        return Center(
        child: Column(

          children: [
            ElevatedButton(
              child: Text("Go to Other ${ctrl.count}"), onPressed: () => Get.to(Other())),
              Wrap(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          width: 50,
          height: 50,
          child:  Center(
            child: Text("title",
            style: TextStyle(color: ctrl.itemColor),),
          )
        ),
        Obx(()
        {
          return DropdownButton<String>(
            value: ctrl.itemValue.toString(),
            hint: Text('请选择'),
            elevation: 1,
            icon: Icon(
              Icons.expand_more,
              size: 20,
              color: ctrl.itemColor,
            ),
            items:ctrl.item
      .map((e) {
        print("items bulid: ${ctrl.item}");
        return DropdownMenuItem<String>(
          value: e,
          child: Text(
            e,
            style: TextStyle(color: ctrl.itemColor),
          ));
      })
      .toList(),
            onChanged: (v)
            {
              print("onCanged v= $v");
              print(ctrl.item);
              ctrl.count++;
              ctrl.itemValue = RxString(v.toString());
            },

            onTap: (){
              print("onTap");
              // setState(() {
                

              //   print("onTap1 ");
              // });
            },
        );
        })
        
      ],
    ),
    ],
          
        )
        );
      }),
      floatingActionButton:
          FloatingActionButton(child: Icon(Icons.add), onPressed: ctrl.updateCom));
  }
}



class Other extends StatelessWidget {
  // 你可以让Get找到一个正在被其他页面使用的Controller，并将它返回给你。
  final Controller c = Get.find();
 
  @override
  Widget build(context){
     // 访问更新后的计数变量
     return Scaffold(body: Center(child: ElevatedButton(
              child: Text("count ${c.count}, back!"), onPressed: () => Get.to(GetxHomePage()))));
  }
}



class Controller extends GetxController{
  var count = 0.obs;
  List item = ["get1", "get2"].obs;
  Color itemColor = Colors.blue;
  RxString itemValue = "get1".obs;
  increment() => count++;

  updateCom()
  {
    item = SerialPort.availablePorts.toList();
    itemValue=RxString(item[0]);
    print(item);
  }
}



//GetBuilder状态管理是对statefulwedit的封装，开销小，手动调用update刷新界面，能够刷新dropdownbutton。
class HomeEsayPage extends StatelessWidget {
  const HomeEsayPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final CounterEasyGetLogic ctrl = Get.put(CounterEasyGetLogic());
    return Scaffold(
      appBar: AppBar(title: const Text('计数器-简单式')),
      body: Center(
        child: GetBuilder<CounterEasyGetLogic>(
          builder: (logicGet)
          {
            return Opacity(
            opacity: ctrl.disable?  0.5:1.0,
            child: DropdownButton<String>(
            value: ctrl.itemValue.toString(),
            hint: Text('请选择'),
            elevation: 1,
            icon: Icon(
              Icons.expand_more,
              size: 20,
              color: ctrl.itemColor,
            ),
            items:ctrl.item
      .map((e) {
        print("items bulid: ${logicGet.item}");
        return DropdownMenuItem<String>(
          value: e,
          child: Text(
            e,
            style: TextStyle(color: ctrl.itemColor),
          ));
      })
      .toList(),
            onChanged: ctrl.disable ? null : (v)
            {
              print("onCanged v= $v");
              print(ctrl.item);
              ctrl.count++;
              ctrl.itemValue = (v.toString());
              ctrl.update();
            },
            
            onTap: (){
              print("onTap");
              // setState(() {
                

              //   print("onTap1 ");
              // });
            },
        ),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => ctrl.updateCom(),
        child: const Icon(Icons.add),
      ),
    );
  }
}

class CounterEasyGetLogic extends GetxController {
  var count = 0;
    List item = ["get1", "get2"];
  Color itemColor = Colors.blue;
  String itemValue = "get1";
  bool disable = false;

  void increase() {
    ++count;
    update();
  }

    updateCom()
  {
    ++count;
    item = SerialPort.availablePorts.toList();
    itemValue=item[0];
    disable = !disable;
    print(item);
    update();
  }
}
