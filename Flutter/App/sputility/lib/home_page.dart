import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:libserialport/libserialport.dart';

//GetBuilder状态管理是对statefulwedit的封装，开销小，手动调用update刷新界面，能够刷新dropdownbutton。
class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final HomePageGetLogic ctrl = Get.put(HomePageGetLogic());
    return Scaffold(
      appBar: AppBar(title: const Text('串口调试助手')),
      body: Container(
        child: MainComSetting(),
      ),

      //悬浮按键
      floatingActionButton: FloatingActionButton(
        onPressed: () => ctrl.updateCom(),
        child: const Icon(Icons.add),
      ),
    );
  }
}

class HomePageGetLogic extends GetxController {
  List item = [""];
  Color itemColor = Colors.blue;
  String itemValue = "";
  bool disable = false;

  List bautRate = ["4800","9600","19200","38400","57600","115200","1000000", "2000000"];

  updateCom() {
    //没有选择有效串口时。检测到串口后默认选择第一个。
    List portList = SerialPort.availablePorts.toList();
    if(portList.isEmpty)
    {
      item = [""];
    }
    else
    {
      item = portList;
      if(itemValue == "")
      {
        itemValue = item[0];
      }
    }

    //disable = !disable;
    print(item);
    update();
  }
}


class MainComSetting extends StatelessWidget {
  const MainComSetting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final HomePageGetLogic ctrl = Get.put(HomePageGetLogic());
    //串口设置栏
    return Container(
      width: 200,
      //填充满父控件
      height: double.infinity,
      
      color: Colors.cyan[100],
      child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          //串口设置栏标题
          Container(
            height:50,
            color: Colors.cyan[400],
            alignment: Alignment.center,
            child: Text(
            '串口设置',
              style: TextStyle(
              fontSize: 25,
              color: Colors.white,
              
              shadows: [
                Shadow(
                  color: Colors.black,
                  offset: Offset(1,1),
                  blurRadius: 10,
                ),
                Shadow(
                  color: Colors.black,
                  offset: Offset(-0.1,0.1),
                  blurRadius: 10,
                )
              ]
            ),
          ),
          ),
           //串口设置栏选项
          Container(
            //内边距
            padding: const EdgeInsets.only(left: 10, right: 0, top: 10, bottom: 0),
            //color: Colors.red,
            child: Row(
            children: <Widget>[
              Text("串口号:  ",
              style: TextStyle(fontWeight: FontWeight.bold),),

              GetBuilder<HomePageGetLogic>(
              builder: (logicGet) {
                return Opacity(
                  opacity: ctrl.disable ? 0.5 : 1.0,
                  child: Container(
                    child: DropdownButton<String>(
                    value: ctrl.itemValue.toString(),
                    hint: Text('请选择'),
                    elevation: 1,
                    icon: Icon(
                      Icons.expand_more,
                      size: 20,
                      color: ctrl.itemColor,
                    ),
                    items: ctrl.item.map((e) {
                      print("items bulid: ${logicGet.item}");
                      return DropdownMenuItem<String>(
                          value: e,
                          child: Text(
                            e,
                            style: TextStyle(color: ctrl.itemColor),
                          ));
                    }).toList(),
                    onChanged: ctrl.disable ? null: (v) {
                            print("onCanged v= $v");
                            print(ctrl.item);
                            ctrl.itemValue = (v.toString());
                            ctrl.update();
                          },
                    onTap: () {
                      print("onTap");
                    },
                  )
                  ), 
                );
              },
            ),
            ]
          )),
          
          Container(
            //内边距
            padding: const EdgeInsets.only(left: 10, right: 0, top: 1, bottom: 0),
            //color: Colors.red,
            child: Row(
            children: <Widget>[
              Text("波特率:  ",
              style: TextStyle(fontWeight: FontWeight.bold),),

              GetBuilder<HomePageGetLogic>(
              builder: (logicGet) {
                return Opacity(
                  opacity: ctrl.disable ? 0.5 : 1.0,
                  child: Container(
                    child: DropdownButton<String>(
                    value: ctrl.itemValue.toString(),
                    hint: Text('请选择'),
                    elevation: 1,
                    icon: Icon(
                      Icons.expand_more,
                      size: 20,
                      color: ctrl.itemColor,
                    ),
                    items: ctrl.item.map((e) {
                      print("items bulid: ${logicGet.item}");
                      return DropdownMenuItem<String>(
                          value: e,
                          child: Text(
                            e,
                            style: TextStyle(color: ctrl.itemColor),
                          ));
                    }).toList(),
                    onChanged: ctrl.disable ? null: (v) {
                            print("onCanged v= $v");
                            print(ctrl.item);
                            ctrl.itemValue = (v.toString());
                            ctrl.update();
                          },
                    onTap: () {
                      print("onTap");
                    },
                  )
                  ), 
                );
              },
            ),
            ]
          )),


        ],

      )
    );
  }
}