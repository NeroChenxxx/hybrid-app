import 'dart:async';
import 'dart:ui';
import 'dart:io';
import 'package:demo1/CustomDropDownButton.dart';
import 'package:demo1/Dashboard.dart';
import 'package:demo1/GetxTest.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'MianWidget.dart';
import 'package:libserialport/libserialport.dart';
import 'package:get/get.dart';
void main() {



  
  //runApp(DashBoard());
  //runApp(MainWidget());
  //runApp(MyApp());

  //runApp( GetMaterialApp(home: GetxHomePage()));
  runApp( GetMaterialApp(home: HomeEsayPage()));
  
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  var value;
  ValueNotifier<List> notify = ValueNotifier<List>([]);

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {

      Timer timer = Timer.periodic(Duration(milliseconds: 1000), (timer) {
      if(Platform.isWindows || Platform.isLinux)
      {
        print('Available ports:');
        var i = 0;
        notify.value=SerialPort.availablePorts.toList();
        // for (final name in SerialPort.availablePorts) {
        //   final sp = SerialPort(name);
        //   notify.value.add(name);
        //   print('${++i}) $name');
        //   // print('\tDescription: ${sp.description}');
        //   // print('\tManufacturer: ${sp.manufacturer}');
        //   // print('\tSerial Number: ${sp.serialNumber}');
        //   // print('\tProduct ID: 0x${sp.productId!.toRadixString(16)}');
        //   // print('\tVendor ID: 0x${sp.vendorId!.toRadixString(16)}');
        //   sp.dispose();
        // }
      }
    });


    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '余额（W）:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
            ValueListenableBuilder(valueListenable: notify, builder: _bulidCom),
            
            
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _bulidCom(BuildContext context, List value, Widget? child) {
    // String s1=value[0];
    print("_bulidCom value = $value  ${value.length} ${value.runtimeType}");

    List a = ["22","33"];
    for ( String name in value)
    {
      a[0] = name;
    }
    print("a = ${a}");
    // var v = ["COM51"];
    // v[0]=value[0].toString();
    // print("_bulidCom v = $s1");
    // List Item2 =  <String>[];
    // Item2.add("com1");
    // print("_bulidCom item2 = $Item2");
    return CustomDropDownButton(itemColor:Colors.red, title:'串口号:', item: a,);
  }
}

