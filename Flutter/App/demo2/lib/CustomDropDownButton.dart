import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:libserialport/libserialport.dart';

class CustomDropDownButton extends StatefulWidget {
  CustomDropDownButton({Key? key, this.itemColor, required this.title,required this.item}) : super(key: key);
  Color? itemColor = Colors.blue;
  String title = "";
  final List item ;
  @override
  _CustomDropDownButtonState createState(){
    print("item1 = $item");
    return _CustomDropDownButtonState(
    itemColor:this.itemColor,
    title:this.title,
    item:this.item,
  );
  }
}

var value;
class _CustomDropDownButtonState extends State<CustomDropDownButton> {
  
  Color? itemColor = Colors.blue;
  String title = "";
  List item ;
  String? itemValue;
  var i = 0;
  Timer? timer = null;

_CustomDropDownButtonState({this.itemColor, required this.title, required this.item});

  @override
  Widget build(BuildContext context) {

    if(timer == null)
    {
      timer = Timer.periodic(Duration(milliseconds: 1000), (timer) {
      setState(() {
        item = SerialPort.availablePorts.toList();
        if(item.length == 0)
        {
          print("length = ${item.length}");
          item.add("无串口");;
        }

        if(!item.contains(itemValue))
        {
          itemValue = item[0];
        }
        print("Timer = $item");
      });
     
    });
    }

    print("item2 = $item");
    return Wrap(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          width: 50,
          height: 50,
          child:  Center(
            child: Text(title,
            style: TextStyle(color: itemColor),),
          )
        ),
        DropdownButton<String>(
            value: itemValue,
            hint: Text('请选择'),
            elevation: 1,
            icon: Icon(
              Icons.expand_more,
              size: 20,
              color: itemColor,
            ),
            items: _buildItems(),
            onChanged: (v)
            {
              setState(() {
              itemValue = v;
              value=v;
              print("onCanged v= $v");
              });
            },

            onTap: (){
              setState(() {
                

                print("onTap1 ");
              });
            },
        )
      ],
    );
  }

  List<DropdownMenuItem<String>> _buildItems() => item
      .map((e) => DropdownMenuItem<String>(
          value: e,
          child: Text(
            e,
            style: TextStyle(color: itemColor),
          )))
      .toList();
}